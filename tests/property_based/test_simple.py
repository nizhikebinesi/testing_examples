import pytest
import hypothesis
from datetime import datetime
from hypothesis import given, strategies as st, settings, Verbosity
from src.simple import get_time_of_day_by_datetime, get_hours_by_time_of_day


@settings(verbosity=Verbosity.verbose)
@given(st.datetimes())
def test_get_time_of_day(dt):
    time_of_day = get_time_of_day_by_datetime(dt)
    hours = get_hours_by_time_of_day(time_of_day)
    assert dt.hour in hours
    assert dt.hour < 13


@settings(verbosity=Verbosity.verbose)
@given(st.datetimes().filter(lambda x: x.hour < 13))
def test_get_time_of_day_with_hours_less_13(dt):
    time_of_day = get_time_of_day_by_datetime(dt)
    hours = get_hours_by_time_of_day(time_of_day)
    assert dt.hour in hours
    assert dt.hour < 13



