import math
import pytest
import hypothesis
from datetime import datetime
from hypothesis import given, strategies as st, settings, Verbosity


def is_sorted(arr):
    for i in range(len(arr) - 1):
        if arr[i] > arr[i + 1]:
            return False
    return True


@given(st.lists(st.integers(0, 3)))
def test_sorted(arr):
    result = sorted(arr)
    # print(arr)
    assert is_sorted(result)


def neg(x):
    return -x


@given(st.integers())
def test_neg(x):
    assert neg(neg(x)) == x


def fizz_buzz(x):
    if not isinstance(x, int):
        raise TypeError("x should be int")
    res = []
    if x % 3 == 0:
        res.append('Fizz')
    if x % 5 == 0:
        res.append('Buzz')
    if len(res) > 0:
        return "".join(res)
    return x


@given(st.integers(min_value=2).filter(lambda x: x % 3 == 0))
def test_fizz_buzz_multiples_3(x):
    result = fizz_buzz(x)
    assert isinstance(result, str) and 'Fizz' in result


@given(st.integers(min_value=2).filter(lambda x: x % 5 == 0))
def test_fizz_buzz_multiples_5(x):
    result = fizz_buzz(x)
    assert isinstance(result, str) and 'Buzz' in result


@given(st.integers(min_value=2).filter(lambda x: x % 3 != 0 and x % 5 != 0))
def test_fizz_buzz_not_multiples_3_and_5(x):
    result = fizz_buzz(x)
    assert isinstance(result, int)
    assert result == x

