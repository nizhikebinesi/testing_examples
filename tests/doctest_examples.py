def f(arr):
    """
    A function for return array

    >>> f([1, 2, 3])
    [1, 2, 3]
    >>> f([2, 4, 5])
    [2, 4,
    5]
    """
    return arr


def g(arr):
    """
    A function for return array

    >>> g([1, 2, 3])
    [1, 2, 3]
    >>> g([2, 4, 5]) # doctest: +NORMALIZE_WHITESPACE
    [2, 4,
    5]
    >>> g([2, 4, 5]) # doctest: +ELLIPSIS
    [2, ..., 5]
    """
    return arr

