from datetime import datetime
from src.simple import get_time_of_day_by_datetime

# datetime(year, month, day, hour, minute, ...)
dt = datetime(2021, 1, 1, 13)
result = get_time_of_day_by_datetime(dt)
print(result)

# dt = datetime(2021, 1, 1, 13)
# result = get_time_of_day_by_datetime(dt)
# expected = "Morning"
# assert result == expected
# assert result == expected, "Expected Morning!"
# assert result == expected, (result, expected)
