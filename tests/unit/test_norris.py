from unittest.mock import Mock
from src.outer_api import NorrisAPI


def test_norris_by_mock():
    outer = Mock()
    r = Mock()
    r.status_code = 200
    r.json.return_value = {
        "type": "success",
        "value": {"id": 1}
    }
    outer.get.return_value = r
    norris = NorrisAPI(outer)
    result, url = norris.get_joke_by_idx(1)

    outer.get.assert_called_once()
    outer.get.assert_called_once_with(url)

    assert result.status_code == 200
    result = result.json()
    assert result.get('type', None) == 'success'
    assert 'value' in result
    assert 'id' in result['value']
    assert result['value']['id'] == 1

