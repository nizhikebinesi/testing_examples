import pytest
from datetime import datetime
from src.simple import get_time_of_day_by_datetime
DATETIMES = [
    datetime(2000, 3, 1, 0),
    datetime(2011, 1, 1, 13),
    datetime(2000, 5, 2, 7),
    datetime(2000, 3, 1, 19),
]
EXPECTED = ['Night', 'Afternoon', 'Morning', 'Evening']

# @pytest.mark.unittest
class TestTimeOfDayForDatetime:
    def test_afternoon(self):
        dt = datetime(2010, 7, 1, 15)
        time_of_day = get_time_of_day_by_datetime(dt)
        assert time_of_day == 'Afternoon'

    def test_returned_value(self):
        dt = datetime(2010, 7, 1, 15)
        time_of_day = get_time_of_day_by_datetime(dt)
        assert isinstance(time_of_day, str)

    def test_wrong_type(self):
        with pytest.raises(TypeError):
            get_time_of_day_by_datetime('123')

    @pytest.mark.parametrize("dt,expected", zip(DATETIMES, EXPECTED))
    def test_time_of_day_parametrize(self, dt, expected):
        time_of_day = get_time_of_day_by_datetime(dt)
        assert time_of_day == expected

