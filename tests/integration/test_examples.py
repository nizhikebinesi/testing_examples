import pytest


def test_tmpfile(tmpfile):
    data = tmpfile.read()
    assert data == 'Hello, World!'


@pytest.mark.parametrize('num', [-1, 42])
def test_tmpfile(tmpfile, num):
    tmpfile.write(str(num), 'a')
    data = tmpfile.read()
    expected = 'Hello, World!' + str(num)
    assert data == expected


def test_db_with_john(prepared_db):
    sql_statement = """
        SELECT * FROM example;
    """
    prepared_db.execute(sql_statement)
    result = prepared_db.fetchall()
    assert len(result) > 0
    assert result[0][0] == "John"

