import pytest
import sqlite3
from src.outer_api import NorrisAPI, OuterAPI
DB_NAME = "database.db"


@pytest.fixture()
def tmpfile(tmpdir):
    p = tmpdir.mkdir("hello").join("world.txt")
    p.write("Hello, World!")
    return p


@pytest.fixture()
def outer_api():
    return OuterAPI()

@pytest.fixture()
def norris(outer_api):
    return NorrisAPI(outer_api)


@pytest.fixture()
def db():
    print("Open connection")
    with sqlite3.connect(DB_NAME) as conn:
        yield conn
    print("Close connection")


@pytest.fixture()
def prepared_db(db):
    print("Prepare database...")
    conn = db
    cursor = conn.cursor()
    sql_statement = """
        CREATE TABLE example
        (name text);
    """
    cursor.execute(sql_statement)
    conn.commit()

    sql_statement = """
        INSERT INTO example
        VALUES ('John');
    """
    cursor.execute(sql_statement)
    conn.commit()
    yield cursor
    sql_statement = """
        DROP TABLE example;
    """
    cursor.execute(sql_statement)
    conn.commit()
    print("Clear database...")

