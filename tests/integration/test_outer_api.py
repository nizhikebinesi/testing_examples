def test_norris_jokes(norris):
    result, _ = norris.get_joke_by_idx(1)
    assert result.status_code == 200
    result = result.json()
    assert result.get('type', None) == 'success'
    assert 'value' in result
    assert 'id' in result['value']
    assert result['value']['id'] == 1

