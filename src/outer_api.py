import requests
BASE_URL = "https://api.icndb.com"


class OuterAPI:
    def get(self, url):
        r = requests.get(url)
        return r


class NorrisAPI:
    def __init__(self, api):
        self.api = api
        self.base_url = BASE_URL
        self.joke_url = self.base_url + "/jokes/"

    def get_joke_by_idx(self, idx):
        url = self.joke_url + str(idx)
        result = self.api.get(url)
        return result, url

