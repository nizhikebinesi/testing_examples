from datetime import datetime


def get_time_of_day_by_datetime(dt):
    """
    A function for returning part of day for datetime

    >>> get_time_of_day_by_datetime(datetime(1999, 12, 31, 0, 0))
    'Night'
    >>> get_time_of_day_by_datetime(datetime(1990, 12, 31, 12, 0))
    'Night'
    """
    if not isinstance(dt, datetime):
        raise TypeError("Wrong type for dt. It should be datetime.datetime")
    hour = dt.hour
    if 5 <= hour < 12:
        return "Morning"
    elif 12 <= hour < 17:
        return "Afternoon"
    elif 17 <= hour < 21:
        return "Evening"
    return "Night"


def get_hours_by_time_of_day(s):
    if s == 'Morning':
        return set(range(5, 12))
    elif s == 'Afternoon':
        return set(range(12, 17))
    elif s == 'Evening':
        return set(range(17, 21))
    return {21, 22, 23, 0, 1, 2, 3, 4}


