# Примеры тестирования на Python

## Первичная настройка
1. Используйте `virtualenv`; 
если вы создавали проект через PyCharm, то 
это можно сделать в окне при создании проекта
    1. (Самостоятельная настройка) `python3 -m venv env`
   
1. (Установка зависимостей) `pip install -r requirements.txt`
1. Чтобы запускать тесты из интерфейса PyCharm нужно выполнить настройку:
    1. `File` -> `Settings` -> `Tools` -> `Python Integrated Tools` -> `Testing`
    1. Сменить в `Testing` значение на `pytest`
    1. Нажать `OK`
    1. Можно запускать тесты символом `Play` рядом с ними

## Запуск тестов
1. Запуск pytest-тестов `python -m pytest`(для всего проекта), чтобы конкретизировать какой файл нужно протестировать достаточно указать путь до него после команды
1. Запуск doctest-тестов `python -m doctest`
1. Запуск doctest-тестов совместно с pytest-like: `python -m pytest --doctest-modules`
1. Чтобы посмотреть на возможные выводы от `print` нужно добавлять `-s -v`

## Дополнительные файлы
1. [ссылка на презентацию](https://docs.google.com/presentation/d/1S4R_-eFGCqGwS40JZStOfIqBukWH-aY1gIgVFhO0cw0/edit?usp=sharing)

## Что читать
- [doctest docs](https://docs.python.org/3/library/doctest.html)
- [pytest docs](https://docs.pytest.org/en/stable/)
- [unittest.mock docs](https://docs.python.org/3/library/unittest.mock.html) 
- [hypothesis docs](https://hypothesis.readthedocs.io/en/latest/)
- [Серия статей по pytest на Хабре](https://habr.com/ru/post/426699/)
- [pytest-html; создание html-отчетов](https://pytest-html.readthedocs.io/en/latest/index.html)
- [allure; тоже html-отчеты, но интерактивнее](https://docs.qameta.io/allure/)
- [Добавление дополнительных полей в отчет](https://habr.com/ru/post/489108/)
- [pytest-testconfig; конфигурации для тестов из файла](https://github.com/wojole/pytest-testconfig)
